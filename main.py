"""
SPDX-FileCopyrightText: 2022 morguldir <morguldir@protonmail.com>
SPDX-License-Identifier: AGPL-3.0-or-later

Gitlab bot to mark merge requests as stale and close them
"""

import dateparser
import logging
import gitlab
import toml
import time
from datetime import datetime, timezone

from gitlab.v4.objects import ProjectMergeRequest
from gitlab.v4.objects.projects import Project

logging.basicConfig(level=logging.INFO)


def mark_stale(project: Project, stale: dict[str, str], stale_labels: list[str], grace_time: datetime) -> None:
    """
    Mark things that have been open for longer than stale_time and has the needs changes label with the stale label
    """
    stale_time = dateparser.parse(stale['since'], settings={'TIMEZONE': '+0000'})
    params = {"not[labels]": "stale"}
    mrs = project.mergerequests.list(state="opened", labels=",".join(stale_labels),
                                     updated_before=str(stale_time), **params)
    for mr in mrs:

        current = datetime.now(timezone.utc)
        grace_date = (current + (current - grace_time)).strftime("%B %d, %Y at %H:%M UTC")
        mr.notes.create({"body": f"""This merge request has the {', '.join(stale_labels)} label(s) \
        and hasn't been updated since {stale['since']}. \\
        It will be closed unless the merge request is updated before {grace_date}"""})

        mr.labels.append("stale")
        mr.save()
        log.info(f"Marked merge request {mr.iid} as stale in {project.name}")


def close_stale(project: Project, grace_time: datetime, stale_labels: list[str]) -> None:
    """
    Close stale issues that have been open longer than grace_time
    """
    labels = ",".join(stale_labels + ["stale"])
    mrs = project.mergerequests.list(state="opened", labels=labels, updated_before=str(grace_time))
    for mr in mrs:
        mr.state_event = 'close'
        mr.save()
        log.info(f"Closed merge request {mr.iid} in project {project.name}")


def remove_stale_label(gl: gitlab.client.Gitlab, project: Project, grace_time: datetime) -> None:
    """
    Remove the stale label if the MR has been updated
    """
    mrs = project.mergerequests.list(labels="stale", updated_after=str(grace_time))
    for mr in mrs:
        # Ignore comments by the bot and only count it as updated if someone else updated it
        user_id = list(gl.personal_access_tokens.list())[0].user_id
        if retrieve_latest_event(user_id, mr, grace_time):
            mr = project.mergerequests.get(mr.iid)
            mr.labels.remove("stale")
            mr.save()
            log.info(f"Removed stale label from merge request {mr.iid} in project {project.name}")


def retrieve_latest_event(user_id: int, mr: ProjectMergeRequest, grace_time: datetime) -> bool:
    """
    Try to get the latest thing that happened from a merge request that wasn't done by the bot
    """
    commits = mr.commits(since=str(grace_time))

    not_author = False
    note_date = None

    if notes := list(mr.notes.list(order_by="updated_at", all=False)):
        note = notes[0]
        not_author = user_id != note.author['id']
        note_date = dateparser.parse(note.updated_at)

    if events := mr.resourcestateevents.list(all=True):
        event = events[-1]
        event_created = dateparser.parse(event.created_at)
        status_update = grace_time < event_created and event.user['id'] != user_id

        event_after = note_date < event_created if note_date else True

        if not commits and (not_author or event_after):
            return status_update

    return not_author or bool(commits)


def run() -> None:
    gl_config = config['gitlab']
    gl = gitlab.client.Gitlab(private_token=gl_config['token'], url=gl_config['repository'])

    stale = config['stale']
    stale_labels = stale['labels']

    project = gl.projects.get(id=gl_config['project_id'])

    grace_time = dateparser.parse(stale['grace'], settings={'TIMEZONE': '+0000', 'RETURN_AS_TIMEZONE_AWARE': True})
    if grace_time is None:
        exit(1)
    remove_stale_label(gl, project, grace_time)
    mark_stale(project, stale, stale_labels, grace_time)
    close_stale(project, grace_time, stale_labels)


if __name__ == '__main__':
    log = logging.getLogger("gitlab-stale-bot")
    sleep = 24 * 60 * 60
    config = toml.load("config.toml")
    while True:
        run()
        log.info("Sleeping...")
        time.sleep(sleep)
