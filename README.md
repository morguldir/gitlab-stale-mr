# Stale Bot for Gitlab Merge Requests

## Installation
`poetry install`

## Usage
 
* Generate a project token for gitlab as shown [here](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html), a personal token should also work

* Copy config.example.toml to config.toml

* Edit config.toml to the desired configuration

* Run the program at main.py, it will do it's thing once a day

* The bot will now automatically mark any merge requests that have the specified label as stale, and then close them after the specified time

